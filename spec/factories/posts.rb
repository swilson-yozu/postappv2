FactoryGirl.define do
	factory :post, class: Post do
		title "MyPost"
		description "MyText"
		user
	end

	factory :post2, class: Post do
		title "MyPost2"
		description "MyText2"
		user
	end

	factory :ruby, class: Post do
		title "A title of a post"
		description "A test description"
		user
	end
end