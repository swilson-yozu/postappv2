require 'rails_helper'

describe Post do
  
  context "post attributes" do 
    it "does not allow attributes to be empty" do
      post = Post.new
      expect(post).to be_invalid
      expect(post.errors[:title]).to be_any
      expect(post.errors[:description]).to be_any
    end
  end

  context "title" do 
    it "is invalid with a duplicated title" do
      user = create(:user)
      create(:ruby, user: user)
      post = build(:ruby, user: user)
      post.valid?
      expect(post.errors[:title]).to include("has already been taken")
    end
  end

  describe "post quota" do
    context "reached daily post quota" do

      it "reaches the post quota" do
        user = create(:user)

        #override the user in post factory
        first_post = create(:post, user: user)
      end
    end
  end
end