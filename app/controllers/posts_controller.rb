class PostsController < ApplicationController
  before_action :set_post, only: [:show, :edit, :update, :destroy]

  # GET /posts
  # GET /posts.json
  def index
    #only showing the current user's posts
    @posts_by_user = Post.where(:user_id => current_user)
    @post_by_date = @posts_by_user.order("created_at DESC").group_by { |i| i.created_at.to_date }
  end

  # GET /posts/new
  def new
    @post = Post.new
  end

  # POST /posts
  # POST /posts.json
  def create
    @post = Post.new(post_params)
    @post.user = current_user

    respond_to do |format|
      if @post.save
        format.js { @posts = Post.all, flash[:notice] = "Post created successfully"}
      else
        format.js { render :new }
      end
    end
  end

  # PATCH/PUT /posts/1
  # PATCH/PUT /posts/1.json
  def update
    respond_to do |format|
      if @post.update(post_params)
        format.js { @posts = Post.all, flash[:notice] = "Post updated successfully"}
      else
        format.js { render :edit}
      end
    end
  end

  # DELETE /posts/1
  # DELETE /posts/1.json
  def destroy
    @post.destroy
    respond_to do |format|
      format.html { redirect_to dashboard_url, notice: 'Post successfully removed.' }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_post
      @post = Post.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def post_params
      params.fetch(:post).permit(:title, :description, :all_tags)
    end
end
