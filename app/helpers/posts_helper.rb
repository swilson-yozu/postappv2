module PostsHelper

	#turns all the tags into html hyperlinks
	def tag_links(tags)
    tags.split(",").map{|tag| link_to tag.strip, tag_path(tag.strip) }.join(" ") 
  end

  def date_check_format(date)
	  if date == Date.today
      "Today"
      elsif date == Date.yesterday
        "Yesterday"
      else
        date.strftime("%d/%m/%y")
    end
  end
end
