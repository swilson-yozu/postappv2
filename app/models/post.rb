class Post < ActiveRecord::Base

  validate :post_quota, :on => :create, if: :user
  validates :title, :description, presence: true
  validates :title, :description, uniqueness: true
  validates :user, presence: true
  belongs_to :user
  has_many :taggings
  has_many :tags, through: :taggings

  #all_tags method to map all tags separated by coma
  def all_tags=(names)
    self.tags = names.split(",").map do |name|
      Tag.where(name: name.strip).first_or_create!
    end
  end

  def all_tags
    self.tags.map(&:name).join(", ")
  end

  def self.tagged_with(name)
    Tag.find_by_name!(name).posts
  end

  private

  def post_quota
    if user.posts.today.count >= 1
      errors.add(:base, "#{user.email} can only post once a day")
    end
  end
end
