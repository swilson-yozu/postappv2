require 'rails_helper'

RSpec.describe User, type: :model do
	
	it "returns email as a string" do
		user = create(:user)
		user.valid?
		expect(user.to_s).to eq("test@abc.com")
	end
end
