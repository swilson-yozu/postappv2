class DashboardController < ApplicationController
  def index
  	@posts = Post.order("created_at DESC")

    if params[:tag]
      @post_by_date = Post.tagged_with(params[:tag]).order("created_at DESC").group_by { |i| i.created_at.to_date }
    else
      @post_by_date = @posts.group_by { |i| i.created_at.to_date }
    end
  end
end